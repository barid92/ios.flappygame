//
//  GameScene.swift
//  FlappyGame
//
//  Created by Bartosz Szewczyk on 12.11.2015.
//  Copyright (c) 2015 Bartosz Szewczyk. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate
{
    var IsGameOver:Bool;
    var IsBirdLive:Bool;
    var bird = SKSpriteNode();
    var pipeUpTexture = SKTexture();
    var pipeDownTexture = SKTexture();
    var PipeMoveAndRemove = SKAction();
    

    
    override init(size: CGSize)
    {
        self.IsGameOver = false;
        self.IsBirdLive = false;
        super.init(size: size);
        
    }
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }

    override func didMoveToView(view: SKView)
    {
        //Physics
        AddPhysics();
        
        //Bird
        AddBird();
        
        //Ground
        AddGround();
      
        //Pipe
        AddPipes();
        
    }
    func SpawnPipes()
    {
        let pipePair = SKNode();
        pipePair.position = CGPointMake(self.frame.size.width + pipeUpTexture.size().width * 1, 0);
        pipePair.zPosition = -10;
        
        let height = UInt32(self.frame.size.height / 4);
        let y = arc4random() % height + height/3;
        
        let pipeDown = SKSpriteNode(texture: pipeDownTexture);
        pipeDown.setScale(2.0);
        pipeDown.position = CGPointMake(0.0, CGFloat(y) + pipeDown.size.height + CGFloat(randomInt(100, max: 200)));
        pipeDown.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(pipeDown.size.width, pipeDown.size.height));
        
        pipeDown.physicsBody?.dynamic = false;
        pipeDown.physicsBody?.categoryBitMask = pipeCategory;
        
        
        let pipeUp = SKSpriteNode(texture: pipeUpTexture);
        pipeUp.setScale(2.0);
        pipeUp.position = CGPointMake(0,CGFloat(y));
        pipeUp.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(pipeUp.size.width, pipeUp.size.height));
        pipeUp.physicsBody?.dynamic = false;
        pipeUp.physicsBody?.categoryBitMask = pipeCategory;
        
        pipePair.addChild(pipeDown);
        pipePair.addChild(pipeUp);
        
        pipePair.runAction(PipeMoveAndRemove);
        self.addChild(pipePair);
        
        
    }
    func AddPipes()
    {
        //Create Pipes
        pipeUpTexture = SKTexture(imageNamed: "PipeUp");
        pipeDownTexture = SKTexture(imageNamed: "PipeDown");
        
        //Movement of pipes
        
        let distanceToMove = CGFloat(self.frame.size.width + 2.0 * pipeUpTexture.size().width);
        let movePipe = SKAction.moveByX(-distanceToMove, y: 0, duration: NSTimeInterval(0.01 * distanceToMove));
        
        let removePipes = SKAction.removeFromParent();
        PipeMoveAndRemove = SKAction.sequence([movePipe,removePipes]);
        
        //Spawn pipes
        
        let spawn = SKAction.runBlock({() in self.SpawnPipes()});
        let delay = SKAction.waitForDuration(NSTimeInterval(2.0));
        let spawnThenDelay = SKAction.sequence([spawn,delay]);
        let spawnThenDelayForever = SKAction.repeatActionForever(spawnThenDelay);
        self.runAction(spawnThenDelayForever);

    }
    func AddPhysics()
    {
        self.physicsWorld.gravity = CGVectorMake(0.0, -5.0);
        self.physicsWorld.contactDelegate = self;
    }
    
    func AddBird()
    {
        IsBirdLive = true;
        let BirdTexture = SKTexture(imageNamed: "Bird");
        let BirdAnimationTexture1 = SKTexture(imageNamed: "frame-1");
        let BirdAnimationTexture2 = SKTexture(imageNamed: "frame-2");
        let BirdAnimationTexture3 = SKTexture(imageNamed: "frame-3");
        let BirdAnimationTexture4 = SKTexture(imageNamed: "frame-4");
        
        let animate = SKAction.sequence([SKAction.animateWithTextures([BirdAnimationTexture1,BirdAnimationTexture2,BirdAnimationTexture3,BirdAnimationTexture4], timePerFrame: 0.1)])
        
        let changeAnimation = SKAction.repeatActionForever(animate);
        
        BirdTexture.filteringMode = SKTextureFilteringMode.Nearest;
        bird = SKSpriteNode(texture: BirdTexture);
        bird.setScale(0.05);
        bird.position = CGPointMake(self.frame.size.width/2,self.frame.size.height + 0.6);
        bird.physicsBody = SKPhysicsBody(circleOfRadius: bird.size.height/2.0);
        bird.physicsBody?.dynamic = true;
        bird.physicsBody?.allowsRotation = false;
        bird.physicsBody?.categoryBitMask = birdCategory;
        bird.physicsBody?.contactTestBitMask = pipeCategory;
        bird.runAction(changeAnimation);
        self.addChild(bird);
    }
    func AddGround()
    {
        let GroundTexture = SKTexture(imageNamed: "ground");
        let  sprite = SKSpriteNode(texture: GroundTexture);
        let position = CGPointMake(self.size.width/2, sprite.size.height/2);
        let size = CGSizeMake(frame.width, GroundTexture.size().height);
        sprite.size = size;
        //sprite.setScale(1.0);
        sprite.position = position;
        self.addChild(sprite);
        
        let ground = SKNode();
        ground.position = position;
        //CGSizeMake(self.frame.size.width, GroundTexture.size().height)
        ground.physicsBody = SKPhysicsBody(rectangleOfSize: size);
        ground.physicsBody?.contactTestBitMask = pipeCategory;
        
        ground.physicsBody?.dynamic = false;
        self.addChild(ground);
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        for touch in touches
        {
            MoveBird(touch);
            if(IsGameOver)
            {
                Restart();
            }
        }
    }
   
    func Restart()
    {
        let newScene = GameScene(size: view!.bounds.size);
        newScene.scaleMode = .AspectFill;
        view!.presentScene(newScene);
    }
    func GameOver()
    {
        IsBirdLive = false;
        IsGameOver = true;
        bird.removeAllActions();
        let tabToGameOverLabel = SKLabelNode(text: "Game Over!")
        tabToGameOverLabel.position = CGPointMake(view!.center.x, view!.center.y + 40);
        tabToGameOverLabel.fontColor = UIColor.blackColor();
        tabToGameOverLabel.fontName = "Helvetica";
        addChild(tabToGameOverLabel);
    }
    func didBeginContact(contact: SKPhysicsContact) {
        GameOver();
    }
    
    func MoveBird(touch: UITouch)
    {
        if  (IsBirdLive)
        {
            _ = touch.locationInNode(self);
            bird.physicsBody?.velocity = CGVectorMake(0, 0);
            bird.physicsBody?.applyImpulse(CGVectorMake(0, 15));
        }
    }
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}

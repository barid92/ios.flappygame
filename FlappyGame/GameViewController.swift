//
//  GameViewController.swift
//  FlappyGame
//
//  Created by Bartosz Szewczyk on 12.11.2015.
//  Copyright (c) 2015 Bartosz Szewczyk. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        var sceen :GameScene;
        let skyView = view as! SKView;
        //skyView.showsPhysics = true;
        skyView.multipleTouchEnabled = false;
        sceen = GameScene(size: skyView.bounds.size);
        sceen.scaleMode = .AspectFill;
        skyView.presentScene(sceen);
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}

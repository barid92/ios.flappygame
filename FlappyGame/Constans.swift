//
//  Constans.swift
//  FlappyGame
//
//  Created by Bartosz Szewczyk on 15.11.2015.
//  Copyright © 2015 Bartosz Szewczyk. All rights reserved.
//

import Foundation
import UIKit

let birdCategory:UInt32 = 0x1 << 0;
let pipeCategory:UInt32 = 0x1 << 1;

func randomInt(min: Int, max: Int) -> Int
{
    return min + Int(arc4random_uniform(UInt32(max - min + 1)));
}